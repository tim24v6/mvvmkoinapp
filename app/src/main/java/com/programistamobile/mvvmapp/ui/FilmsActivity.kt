package com.programistamobile.mvvmapp.ui
// based on github.com/programistamobile/mvvm-app-android Kamil Krzysztof
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.programistamobile.mvvmapp.R
import com.programistamobile.mvvmapp.domain.FilmModel
import kotlinx.android.synthetic.main.activity_films.*
//import com.programistamobile.mvvmapp.databinding.ActivityFilmsBinding

// what is this?
typealias OnRemoveFilmListener = (filmModel: FilmModel) -> Unit

class FilmsActivity : AppCompatActivity() {

//    private lateinit var binding: ActivityFilmsBinding
    private val viewModel: FilmsViewModel by viewModels()

    private val adapter: FilmsAdapter by lazy {
        FilmsAdapter(
            onRemoveFilmListener = { viewModel.removeFilm(it) }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        binding = ActivityFilmsBinding.inflate(layoutInflater)
//        val view = binding.root
        setContentView(R.layout.activity_films)

        initRecyclerView()
        initListeners()
        initObservers()
    }

    private fun initRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    private fun initObservers() {
        viewModel.fetchAllFilmsLiveData().observe(this, Observer { films ->
            films?.let {
                adapter.setItems(films)
            }
        })
    }

    private fun initListeners() {
        saveButton.setOnClickListener {
            viewModel.storeFilm(titleInput.text.toString())
        }
    }
}